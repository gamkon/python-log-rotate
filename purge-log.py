#!/bin/python3
# Log rotation script by GamKon
import shutil   # For CopyFile
import os       # For GetFileSize, Check if file exist
import sys      # For CLI arguments

# Run parameters:
# python3 purge-log.py my_log.txt 10 5

# Check for input arguments
if (len(sys.argv) < 4):
    print("Missing arguments. Arguments: log_file size_limit logs_count_to_keep")
    exit(1)

log_file_name         = sys.argv[1]
log_size_limit        = int(sys.argv[2])
logs_count_to_keep    = int(sys.argv[3])

# some debug info to print
# print(log_file_name, " ", log_size_limit, " ", logs_count_to_keep)

if (os.path.isfile(log_file_name) == True):
    log_file_size = os.stat(log_file_name).st_size // 1024  # Get size in KiloBytes
    print("Log file size: ", log_file_size)
    
    if (log_file_size >= log_size_limit):
        if (logs_count_to_keep > 0):
            for current_file_index in range(logs_count_to_keep, 1, -1):
                src_file  = log_file_name + "-" + str(current_file_index - 1)
                dest_file = log_file_name + "-" + str(current_file_index)
                if (os.path.isfile(src_file)):    
                    print ("copy ", src_file, " -> ", dest_file)
                    shutil.copyfile(src_file, dest_file)
            shutil.copyfile(log_file_name, log_file_name + "-1")
            print("Copiyed ", log_file_name, " to ", log_file_name+"-1")
            
        # Clear main log file
        my_file = open(log_file_name, "w")
        my_file.close()
    else:
        print("Log file is less than", log_size_limit, "Kb, skipping.")            
        